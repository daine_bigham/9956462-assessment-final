﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MySql.Data.MySqlClient;
using MySQLDemo.Classes;
using Windows.UI.Popups;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SplitViewDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HomePage : Page
    {
        public HomePage()
        {
            this.InitializeComponent();

            names.ItemsSource = loadAllTheData();
        }

        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}' ";
            var b = MySQLCustom.ShowInList(command);

            userAccount.Text = b[0];
            FirstName.Text = b[1];
            SecondName.Text = b[2];
            DOB.Text = b[3];
            StringName.Text = b[4];
            StringNumber.Text = b[5];
            Postcode.Text = b[6];
            PhoneOne.Text = b[7];
            PhoneTwo.Text = b[8];
            Email.Text = b[9];
        }

        static List<string> loadAllTheData()
        {
            var command = $"Select FNAME from tbl_people";

            var list = MySQLCustom.ShowInList(command);

            return list;
        }

        static async void messageBox(string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = "Checking value";

            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
            { }
        }

        private void ClearText_Click(object sender, RoutedEventArgs e)
        {
            FirstName.Text = "";
            SecondName.Text = "";
            StringName.Text = "";
            StringNumber.Text = "";
            Postcode.Text = "";
            City.Text = "";
            PhoneOne.Text = "";
            PhoneTwo.Text = "";
            Email.Text = "";
            userAccount.Text = "";
        }

        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (names.SelectedItem != null)
            {
                selectedName(names.SelectedItem.ToString());
            }
        }

        private void EnterContact_Click(object sender, RoutedEventArgs e)
        {     
            MySQLCustom.AddData(FirstName.Text, SecondName.Text, DOB.Text, StringName.Text, StringNumber.Text, Postcode.Text, City.Text, PhoneOne.Text, PhoneTwo.Text, Email.Text);
            names.ItemsSource = loadAllTheData();
            
        }

        private void RemoveCOntact_Click(object sender, RoutedEventArgs e)
        {
            MySQLCustom.DeleteDate(userAccount.Text);
            names.ItemsSource = loadAllTheData();
        }
    }
}
